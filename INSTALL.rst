Proxi
=====

Proxi is a Python package for proximity graph construction. In proximity graphs, each node is connected by an
edge (directed or undirected) to its nearest neighbors according to some distance metric *d*.


Installation
============

Dependencies
------------

Proxi requires:

Python (>= 2.7 or >= 3.3)
NumPy (>= 1.8.2)
SciPy (>= 0.13.3)
NetworkX (>= 2.1)
Sklearn (>= 0.19.1)


User installation
-----------------

If you already have a working installation of the required packages, the easiest way to install proxi is using pip:

    $ pip install proxi

To upgrade to a newer release use the ``--upgrade`` flag:

    $ pip install --upgrade proxi

