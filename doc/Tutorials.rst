Tutorials
=========

Example simple uses and applications of Proxi are provided in the following tutorials


.. toctree::
   :maxdepth: 1

   ./tutorials/proxi_example_1.ipynb
   ./tutorials/proxi_example_2.ipynb
   ./tutorials/proxi_example_3.ipynb
   ./tutorials/proxi_example_4.ipynb
