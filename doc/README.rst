ReadMe
======

Proxi is a Python package for proximity graph construction. In proximity graphs, each node is connected by an
edge (directed or undirected) to its *k* nearest neighbors according to some distance metric *d*.


- **Website:** http://idsrlab.com/proxi/
- **Documentation:** https://proxi.readthedocs.io/en/latest/index.html
- **Tutorials:** https://proxi.readthedocs.io/en/latest/Tutorials.html
- **Source:** https://bitbucket.org/idsrlab/proxi/


Install
-------

Install the latest version of Proxi::

    $ pip install proxi

For additional details, please see 'INSTALL.rst'.


Bugs
----

Please report any bugs that you find `here <mailto:yasser@idsrlab.com>`_.

License
-------

Proxi is released under the 3-Clause BSD license (see 'LICENSE.txt').
Copyright (C) 2018 Yasser El-Manzalawy <yasser@idsrlab.com>