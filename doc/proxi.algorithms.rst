proxi.algorithms package
========================

Submodules
----------

proxi.algorithms.knng module
----------------------------

.. automodule:: proxi.algorithms.knng
    :members:
    :undoc-members:
    :show-inheritance:

proxi.algorithms.pairwise module
--------------------------------

.. automodule:: proxi.algorithms.pairwise
    :members:
    :undoc-members:
    :show-inheritance:

proxi.algorithms.pknng module
-----------------------------

.. automodule:: proxi.algorithms.pknng
    :members:
    :undoc-members:
    :show-inheritance:

proxi.algorithms.rng module
---------------------------

.. automodule:: proxi.algorithms.rng
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: proxi.algorithms
    :members:
    :undoc-members:
    :show-inheritance:
