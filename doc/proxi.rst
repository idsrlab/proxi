proxi package
=============

Subpackages
-----------

.. toctree::

    proxi.algorithms
    proxi.utils

Module contents
---------------

.. automodule:: proxi
    :members:
    :undoc-members:
    :show-inheritance:
