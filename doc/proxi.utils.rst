proxi.utils package
===================

Submodules
----------

proxi.utils.distance module
---------------------------

.. automodule:: proxi.utils.distance
    :members:
    :undoc-members:
    :show-inheritance:

proxi.utils.misc module
-----------------------

.. automodule:: proxi.utils.misc
    :members:
    :undoc-members:
    :show-inheritance:

proxi.utils.process module
--------------------------

.. automodule:: proxi.utils.process
    :members:
    :undoc-members:
    :show-inheritance:

proxi.utils.similarity module
-----------------------------

.. automodule:: proxi.utils.similarity
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: proxi.utils
    :members:
    :undoc-members:
    :show-inheritance:
