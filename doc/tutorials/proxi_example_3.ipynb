{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How to construct an aggregated kNN graph?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "by Yasser El-Manzalawy <yasser@idsrlab.com>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<hr>\n",
    "\n",
    "In tutorial 1, we showed how to construct a kNN graph. To construct such graphs, you need to decide on k (number of neighbors) and <i>d</i> (the dissimilarity metric).  Selecting a dissimilarity metric is not trivial and should be taken into account when interpreting the resulting kNN graph. Proxi allows the following distance functions to be used:\n",
    "\n",
    "        - from scikit-learn: ['cityblock', 'cosine', 'euclidean', 'l1', 'l2', 'manhattan']\n",
    "\n",
    "        - from scipy.spatial.distance: ['braycurtis', 'canberra', 'chebyshev',\n",
    "          'correlation', 'dice', 'hamming', 'jaccard', 'kulsinski',\n",
    "          'mahalanobis', 'matching', 'minkowski', 'rogerstanimoto',\n",
    "          'russellrao', 'seuclidean', 'sokalmichener', 'sokalsneath',\n",
    "          'sqeuclidean', 'yule']\n",
    "\n",
    "        - any callable function (e.g., distance functions in proxi.utils.distance module)\n",
    "        \n",
    "Moreover, Proxi supports any user-defined callable function. For example, a user might define a new function that is the average or weighted combination of some of the functions listed above. Finally, Proxi aggregate_graphs and filter_edges_by_votes methods allow user to construct different kNN graphs using different distance functions and/or <i>k</i>s. In what follows, we show how to aggregate three graphs constructed using <i>k</i>=5 and three different distance functions.\n",
    "<hr>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import networkx as nx\n",
    "\n",
    "from proxi.algorithms.knng import get_knn_graph\n",
    "from proxi.utils.misc import save_graph, save_weighted_graph, aggregate_graphs, filter_edges_by_votes\n",
    "from proxi.utils.process import *\n",
    "from proxi.utils.distance import abs_correlation, abs_spearmann, abs_kendall\n",
    "\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Input file(s)\n",
    "healthy_file = './data/L6_healthy_train.txt'   # OTU table\n",
    "\n",
    "# Output file(s)\n",
    "healthy_graph_file = './graphs/L6_healthy_train_aknng.graphml'   # Output file for aggregated pkNN graphs\n",
    "\n",
    "# Graph aggregation parameters\n",
    "num_neighbors = 5  # Number of neighbors, k, for kNN graphs\n",
    "dists = [abs_correlation, abs_spearmann, abs_kendall]   # distance functions\n",
    "min = 2      # minimum number of edges needed to have an edge in the aggregated graph\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load OTU Table and remove useless OTUs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load OTU Table\n",
    "df = pd.read_csv(healthy_file, sep='\\t')\n",
    "\n",
    "# Proprocess OTU Table by deleting OTUs with less than 5% non-zero values\n",
    "df = select_top_OTUs(df, get_non_zero_percentage, 0.05, 'OTU_ID')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Method 1 for constructing an undirected aggregated kNN graph "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "graphs = []\n",
    "# Construct kNN-graphs using different distance fucntions\n",
    "for dist in dists:\n",
    "    nodes, a = get_knn_graph(df, k=num_neighbors, metric=dist)\n",
    "    graphs.append(a.todense())\n",
    "    \n",
    "aggregated_graph,_ = aggregate_graphs(graphs, min)\n",
    "\n",
    "# Save the constructed graph in an edge list format\n",
    "save_graph(aggregated_graph, nodes, healthy_graph_file)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we can visualize the graph using Cytoscape (See Fig. 1)\n",
    "![title1](./imgs/T3_Fig1.jpg)\n",
    "<b>Figure 1: Aggregated kNN graph obtained by aggregating three kNN graphs consutucted using three distance functions, abs_correlation, abs_spearmann, and abs_kendall.</b>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An interesting property of the aggregated graph in Fig. 1 is that each edge is supported by at least 2 distance functions. Alternatively, one can aggregate the three graphs such that each edge is supported by one <i>fixed</i> base distance function (e.g., abs_correlation) plus at least one of the remaining two functions. Therefore, each edge in the resulting aggregated graph (Fig. 2) is supported by at least two functions such that one of them is abs_correlation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Method 2 for constructing an undirected aggregated kNN graph "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Specify input/output files and parameters\n",
    "\n",
    "# Output file\n",
    "healthy_graph_file2 = './graphs/L6_healthy_aknng2.graphml'   # Output file for aggregated pkNN graphs\n",
    "\n",
    "# Graph aggregation parameters\n",
    "base_distance = abs_correlation\n",
    "dists = [abs_spearmann, abs_kendall]   # distance functions\n",
    "min_votes = 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "graphs = []\n",
    "# Construct kNN-graphs using different distance fucntions\n",
    "for dist in dists:\n",
    "    nodes, a = get_knn_graph(df, k=num_neighbors, metric=dist)\n",
    "    graphs.append(a.todense())\n",
    "\n",
    "nodes, a = get_knn_graph(df, k=num_neighbors, metric=base_distance)\n",
    "aggregated_graph,_ = filter_edges_by_votes(a.todense(), graphs, min)\n",
    "\n",
    "# Save the constructed graph in an edge list format\n",
    "save_graph(aggregated_graph, nodes, healthy_graph_file2)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![title2](./imgs/T3_Fig2.jpg)\n",
    "<b>Figure 2: Sparse base kNN graph (using abs_correlation) and remaining two graphs are used for filtering out unsupported edges.</b>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It worths to mention that these two methods of aggregating graphs could also be applied to aggregate the following graphs:\n",
    "<ul>\n",
    "    <li> kNN graphs constructed with different <i>k</i> values</li>\n",
    "    <li> radius graphs rNN graphs with different <i>radius</i> values</i>\n",
    "    <li> different perturbed kNN graphs obtained using different T, c, k, or distance parameters</li>\n",
    "</ul>\n",
    "        "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
