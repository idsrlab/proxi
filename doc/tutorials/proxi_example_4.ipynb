{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Comparative network analysis of perturbed kNN graphs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "by Yasser El-Manzalawy <yasser@idsrlab.com>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<hr>\n",
    "\n",
    "In this tutorial, we construct two perturbed kNN graph for IBD and healthy controls (respectively) and then present examples of possible comparative network analysis that could be apply to the two graphs using Cytoscape. In particular, we compare the two graphs using:\n",
    "- Their global topological properties obtained using Cytoscape NetworkAnalyzer tool\n",
    "- Their top modules obtained using MCODE plugins\n",
    "- Their most varying nodes using DyNet Analyzer plugins and we report the subnetwork of top most varying 20 nodes   (potential IBD biomarkers)\n",
    "\n",
    "<hr>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import networkx as nx\n",
    "\n",
    "from proxi.algorithms.pknng import get_pknn_graph\n",
    "from proxi.utils.misc import save_graph, save_weighted_graph\n",
    "from proxi.utils.process import *\n",
    "from proxi.utils.distance import abs_correlation\n",
    "\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Construct an undirected pkNN graph using IBD OTU table"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Input file(s)\n",
    "ibd_file = './data/L6_IBD_train.txt'   # OTU table\n",
    "\n",
    "# Ouput file(s)\n",
    "ibd_graph_file = './graphs/L6_IBD_train_pknng.graphml'   # Output file for pkNN graph\n",
    "\n",
    "# Parameters\n",
    "num_neighbors = 5        # Number of neighbors, k, for kNN graphs\n",
    "dist = abs_correlation   # distance function\n",
    "T=100                    # No of iterations\n",
    "c=0.6                    # control parameter for pknng algorithm"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Shape of original data is (178, 200)\n"
     ]
    }
   ],
   "source": [
    "# Load OTU Table\n",
    "df = pd.read_csv(ibd_file, sep='\\t')\n",
    "\n",
    "# Proprocess OTU Table by deleting OTUs with less than 5% non-zero values\n",
    "df = select_top_OTUs(df, get_non_zero_percentage, 0.05, 'OTU_ID')\n",
    "\n",
    "# Construct kNN-graph\n",
    "nodes, a,_ = get_pknn_graph(df, k=num_neighbors, metric=dist, T=T, c=c)\n",
    "\n",
    "# Save the constructed graph in an edge list format\n",
    "save_graph(a, nodes, ibd_graph_file)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Fig. 1 shows the constructed perturbed kNN graph from IBD samples.\n",
    "![title1](./imgs/T4_Fig1.jpg)\n",
    "<b>Figure 1: Perturbed kNN undirected proximity graph constructed from IBD OTU table using k=5, T=100, and c=0.6.</b>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Fig. 2 shows the constructed perturbed kNN graph from healthy control samples. Note that we don't need to construct this network since it has been generated in tutorial 2.\n",
    "![title2](./imgs/T4_Fig2.jpg)\n",
    "<b>Figure 2: Perturbed kNN undirected proximity graph constructed from healthy OTU table using k=5, T=100, and c=0.6 (See Example_2).</b>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we can use cytoscape and some of its plugins to compare the two graphs in Figures 1 and 2."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###  Analysis of global topological properties"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we used Cytoscape NetworkAnalyzer tool (1) to get several global properties of each network. Fig. 3 shows that IBD network has higher average node degree, clustering coefficient, network centralization, and number of nodes.\n",
    "\n",
    "![title3](./imgs/T4_Fig3.jpg)\n",
    "<b>Figure 3: Global network properties for healthy (top) and IBD (bottom) networks.</b>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Analysis of top first modules"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Second, we used MCODE (2) to extract top modules from each network. Fig. 4 compare the top first module from healthy (top) and IBD (bottom) networks. For healthy network, the top module includes interactions between 4 different genera of  Firmicutes and 2 different genera of Actionbacteria. For IBD network, the top module includes interactions among different genara belonging to Actionbacteria, Proteobacteria, Firmicutes, and Bacteriodetes phylum.\n",
    "\n",
    "![title4](./imgs/T4_Fig4.jpg)\n",
    "<b>Figure 4: Top module extracted from healthy (top) and IBD (bottom) networks.</b>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Analysis of most varying nodes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Third, we used DyNet Analyzer (3) to compare the the networks in healthy and IBD states. The results are visualized in Fig. 5 where: green edges represent edges present only in healthy network; red edges represent edges present only in IBD network; and gray edges represent edges present in both networks. DyNet also associates a rewiring score with each node that quantifies the amount of change in the identity of the node interacting neighbors. We then ranked nodes by their DyNet score and generated a subnetwork of the top 20 nodes (See Fig. 6). Interestingly, 13 out of 20 nodes form a single connected module. In this module, two nodes corresponding to corynebacterium genera and Rhodocyclaceae family have the highest node degrees of 5 and 4 (respectively).\n",
    "![title5](./imgs/T4_Fig5.jpg)\n",
    "<b>Figure 5: DynNet Analyzer. Healthy (green) and IBD (red).</b>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![title6](./imgs/T4_Fig6.jpg)\n",
    "<b>Figure 6: Subnetwork of top 20 varying nodes determined using DyNet score.</b>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>References:</b><br>\n",
    "\n",
    "[1] Assenov, Yassen, et al. \"Computing topological parameters of biological networks.\" Bioinformatics 24.2 (2007):    282-284.<br>\n",
    "\n",
    "[2] Bader, Gary D., and Christopher WV Hogue. \"An automated method for finding molecular complexes in large protein \n",
    "interaction networks.\" BMC bioinformatics 4.1 (2003): 2.<br>\n",
    "\n",
    "[3] Goenawan, Ivan H., Kenneth Bryan, and David J. Lynn. \"DyNet: visualization and analysis of dynamic molecular \n",
    "interaction networks.\" Bioinformatics 32.17 (2016): 2713-2715.<br>\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
